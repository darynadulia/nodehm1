const express = require('express');
const morgan = require('morgan');
const app = express();

const filesRouter = require('./filesRouter');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/files', filesRouter);

app.listen(8080);
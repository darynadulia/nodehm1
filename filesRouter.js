const fs = require('fs').promises;
const express = require('express');
const filesRouter = express.Router();

let createdFiles = [];
(async () => {
    const list = await fs.readFile('list.json', 'utf-8');
    if (list) {
        createdFiles = JSON.parse(list).createdFiles;
    }
})();

// get list of files
filesRouter.get('/', (req, res) => {
    if(createdFiles.length === 0){
        return res.status(404).json({message: "Client error"});
    }
    res.json({ message: "Success", files: createdFiles });
});

// create new file
filesRouter.post('/', async (req, res) => {
    const { filename, content } = req.body;

    if (!filename) {
        return res.status(400).json({ "message": "Please specify 'filename' parameter" });
    }

    const supportedFormats = ['log', 'txt', 'json', 'yaml', 'xml', 'js'];
    if (!supportedFormats.includes(filename.split('.').pop())) {
        return res.status(400).json({ "message": "Please specify another file format" });
    }
    
    if (!content) {
        return res.status(400).json({ "message": "Please specify 'content' parameter" });
    }
    await fs.writeFile(filename, content);
        if (!createdFiles.includes(filename)) {
            createdFiles.push(filename);
            await fs.writeFile('list.json', JSON.stringify({ createdFiles }));
        }
        res.json({ "message": "File created successfully" });
});

// get file information
filesRouter.get('/:filename', async(req, res) => {
    const filename = req.params.filename;
    try{
        const content = await fs.readFile(filename, 'utf-8');
        const extension = filename.split('.').pop();
        const { birthtime: uploadedDate } = await fs.stat(filename);
        res.json({
            "message": "Success",
            filename,
            content,
            extension,
            uploadedDate
        });
    }
    catch(error){
        return res.status(400).json({ "message": "No file with " + filename + " filename found" });
    }        
});

// modify file
filesRouter.patch('/:filename', async (req, res) => {
    const filename = req.params.filename;
    const { content } = req.body;
    if (!filename) {
        return res.status(400).json({ "message": "Please specify a filename"});
    }
    if (!content) {
        return res.status(400).json({ "message": "Please specify a content" });
    }
    try{
        await fs.writeFile(filename, content);
        res.json({ message: "Success" });
    }
    catch(error){
        return res.status(400).json({ "message": "No file with " + filename + " filename found" });
    }
});

// delete file
filesRouter.delete('/:filename', async (req, res) => {
    const filename = req.params.filename;
    if (!filename) {
        return res.status(400).json({ "message": "Please specify a filename" });
    }
    try{
        await fs.unlink(filename);
        createdFiles.splice(createdFiles.indexOf(filename), 1);
        await fs.writeFile('list.json', JSON.stringify({ createdFiles }));
        res.json({ "message": "Deleted successfully" });
    }
    catch(error){
        res.json(404).json({ "message": "No file with that name was found" });
    }
});

module.exports = filesRouter;